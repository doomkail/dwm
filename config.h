/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:pixelsize=14", "JoyPixels:pixelsize=12:antialias=true:autohint=true"  };
static char dmenufont[]       = "monospace:size=12";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
/* tagging */
static const char *tags[] = { "🌎", "🌍", "", "", "", "🎮", "", "", "🖧" };

/* static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }; */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                   instance     title       tags mask     switchtotag    isfloating   isterminal    noswallow   monitor */
	{ "Brave",                  NULL,       NULL,       1,                  1,           0,            0,         0,        -1 },
	{ "Firefox",                NULL,       NULL,       1 << 1,             1,           0,            0,         0,        -1 },
	{ "firefox",                NULL,       NULL,       1 << 1,             1,           0,            0,         0,        -1 },
	{ "Pcmanfm",                NULL,       NULL,       1 << 2,             1,           0,            0,         0,        -1 },
	{ "discord",                NULL,       NULL,       1 << 3,             1,           0,            0,         0,        -1 },
	{ "Telegram",               NULL,       NULL,       1 << 3,             1,           0,            0,         0,        -1 },
	{ "Element",                NULL,       NULL,       1 << 3,             1,           0,            0,         0,        -1 },
	{ "Signal",                 NULL,       NULL,       1 << 3,             1,           0,            0,         0,        -1 },
	{ "Tenacity",               NULL,       NULL,       1 << 4,             1,           0,            0,         0,        -1 },
	{ "St",                     NULL,   "ncmpcpp",      1 << 4,             1,           0,            1,         0,        -1 },
        { "St",                     NULL,   "castero",      1 << 4,             1,           0,            1,         0,        -1 },
	{ "spotify",                NULL,       NULL,       1 << 4,             1,           0,            0,         0,        -1 },
	{ "heroic",                 NULL,       NULL,       1 << 5,             1,           0,            0,         0,        -1 },
	{ "Lutris",                 NULL,       NULL,       1 << 5,             1,           0,            0,         0,        -1 },
	{ "yuzu",                   NULL,       NULL,       1 << 5,             1,           0,            0,         0,        -1 },
	{ "Steam",                  NULL,       NULL,       1 << 5,             1,           0,            0,         0,        -1 },
	{ "Wine",                   NULL,       NULL,       1 << 5,             1,           0,            0,         0,        -1 },
	{ "Wine-staging",           NULL,       NULL,       1 << 5,             1,           0,            0,         0,        -1 },
	{ "LBRY",                   NULL,       NULL,       1 << 6,             1,           0,            0,         0,        -1 },
	{ "kdenlive",               NULL,       NULL,       1 << 6,             1,           0,            0,         0,        -1 },
	{ "Komikku",                NULL,       NULL,       1 << 6,             1,           0,            0,         0,        -1 },
	{ "qBittorrent",            NULL,       NULL,       1 << 7,             1,           0,            0,         0,        -1 },
	{ "Joplin",                 NULL,       NULL,       1 << 7,             1,           0,            0,         0,        -1 },
	{ "tlauncher",              NULL,       NULL,       1 << 7,             1,           0,            0,         0,        -1 },
	{ "teams",                  NULL,   "Microsoft Teams",       1 << 7,             1,           0,            0,         0,        -1 },
	{ "Minecraft",              NULL,       NULL,       1 << 7,             1,           0,            0,         0,        -1 },
	{ "obs",                    NULL,       NULL,       1 << 8,             1,           0,            0,         0,        -1 },
	{ "Gimp",                   NULL,       NULL,       1 << 8,             1,           0,            0,         0,        -1 },
	{ "Virt-manager",           NULL,       NULL,       1 << 8,             1,           0,            0,         0,        -1 },
	{ "St",                     NULL,       NULL,       0,                  0,           0,            1,         0,        -1 },
	{ "St",                     NULL,      "htop",      1 << 8,             1,           0,            1,         0,        -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
/* "-m", dmenumon, */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, "-p", "run:"};
static const char *termcmd[]  = { "st", NULL };

#include <X11/XF86keysym.h>
#include "shiftview.c"
#include <X11/XF86keysym.h>
static Key keys[] = {
	/* modifier                     key        function        argument */
	/* STACKKEYS(MODKEY,                          focus) */
	/* STACKKEYS(MODKEY|ShiftMask,                push) */
	{ MODKEY,                       XK_grave,           focusmon,       {.i = +1 } },
       	{ MODKEY|ShiftMask,             XK_grave,           tagmon,         {.i = +1 } },
	TAGKEYS(			XK_1,		0)
	TAGKEYS(			XK_2,		1)
	TAGKEYS(			XK_3,		2)
	TAGKEYS(			XK_4,		3)
	TAGKEYS(			XK_5,		4)
	TAGKEYS(			XK_6,		5)
	TAGKEYS(			XK_7,		6)
	TAGKEYS(			XK_8,		7)
	TAGKEYS(			XK_9,		8)
	{ MODKEY,			XK_0,		view,		{.ui = ~0 } },
	{ MODKEY|ShiftMask,		XK_0,		tag,		{.ui = ~0 } },
	{ MODKEY,			XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,			XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,			XK_BackSpace,	spawn,		SHCMD("sysact") },
	/* { MODKEY|ShiftMask,		XK_BackSpace,	spawn,		SHCMD("") }, */

	{ Mod1Mask,			XK_Tab,		shiftview,	{ .i = +1 } },
	{ Mod1Mask|ShiftMask,   	XK_Tab,		shiftview,	{ .i = -1 } },

	{ MODKEY,                       XK_Tab,         cyclelayout,    {.i = +1} },
	{ MODKEY|ShiftMask,             XK_Tab,         cyclelayout,    {.i = -1 } },

	/* { MODKEY|ShiftMask,	        XK_q,		quit,	{0} }, */
	{ MODKEY|ControlMask,	        XK_q,		quit,	{0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,           quit,   {1} },

	{ MODKEY,			XK_w,		spawn,		SHCMD("$BROWSER") },
	{ MODKEY|ShiftMask,		XK_w,		spawn,		SHCMD("st -A sudo nmtui") },
	{ MODKEY,			XK_e,		spawn,		SHCMD("pcmanfm") },
	{ MODKEY|ShiftMask,		XK_e,		spawn,		SHCMD("st -e vifm") },

	/* { MODKEY,			XK_r,		spawn,		SHCMD("st -e lf") }, */
	/* { MODKEY|ShiftMask,		XK_r,		quit,		{1} }, */

	{ MODKEY,			XK_t,			spawn,		SHCMD("") },
	/* { MODKEY|ShiftMask,		XK_t,			spawn,		SHCMD("flatpak run org.telegram.desktop") }, */
	{ MODKEY|ShiftMask,		XK_t,			spawn,		SHCMD("telegram-desktop") },
	{ MODKEY,			XK_p,			spawn,		SHCMD("mpc toggle") },
	{ MODKEY|ShiftMask,		XK_p,			spawn,		SHCMD("mpc pause ; pauseallmpv") },
	{ MODKEY,			XK_bracketleft,		spawn,		SHCMD("mpc seek -10") },
	{ MODKEY|ShiftMask,		XK_bracketleft,		spawn,		SHCMD("mpc seek -60") },
	{ MODKEY,			XK_bracketright,	spawn,		SHCMD("mpc seek +10") },
	{ MODKEY|ShiftMask,		XK_bracketright,	spawn,		SHCMD("mpc seek +60") },
	{ MODKEY,			XK_backslash,		view,		{0} },
	/* { MODKEY|ShiftMask,		XK_backslash,		spawn,		SHCMD("") }, */

	{ MODKEY,			XK_a,		spawn,		SHCMD("st -e pulsemixer; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		XK_a,		spawn,		SHCMD("android-studio") },
	{ MODKEY,			XK_s,		togglesticky,	{0} },
	/* { MODKEY|ShiftMask,		XK_s,		spawn,		SHCMD("") }, */
	{ MODKEY,			XK_d,		spawn,          {.v = dmenucmd } },
	/* { MODKEY|ShiftMask,		XK_d,		spawn,		SHCMD("dmenu") }, */
	/* { MODKEY|ShiftMask,		XK_d,		togglegaps,	{0} }, */
	{ MODKEY,			XK_f,		togglefullscr,	{0} },
	{ MODKEY|ShiftMask,		XK_f,		setlayout,	{.v = &layouts[1]} },
	{ MODKEY,			XK_g,		spawn,  	SHCMD("lutris") },
	/* { MODKEY|ShiftMask,		XK_g,		spawn,		SHCMD("") }, */

	{ MODKEY,			XK_o,		incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,		XK_o,		incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_j,           focusstack,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_j,           rotatestack,    {.i = +1 } },
	{ MODKEY,                       XK_k,           focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_k,           rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_h,           setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,           setmfact,       {.f = +0.05} },

	{ MODKEY,                       XK_Return,      spawn,          {.v = termcmd } },
	/* { MODKEY,			XK_z,		spawn,		SHCMD("") }, */
	{ MODKEY,			XK_z,		spawn,		SHCMD("") },
	/* { MODKEY|ShiftMask,		XK_z,		spawn,		SHCMD("flatpak run us.zoom.Zoom ") }, */
	{ MODKEY|ShiftMask,		XK_z,		spawn,		SHCMD("zoom") },
	/* { MODKEY,			XK_x,		spawn,		SHCMD("flatpak run io.lbry.lbry-app") }, */
	{ MODKEY,			XK_x,		spawn,		SHCMD("lbry") },
	{ MODKEY|ShiftMask,		XK_x,		spawn,		SHCMD("") },
	{ MODKEY,			XK_c,		spawn,		SHCMD("qalculate-gtk") },
	{ MODKEY|ShiftMask,             XK_c,           killclient,     {0} },
	{ MODKEY,			XK_v,		spawn,		SHCMD("sudo -A virt-manager") },
	/* { MODKEY|ShiftMask,		XK_v,		spawn,		SHCMD("flatpak run com.vscodium.codium") }, */
	{ MODKEY,			XK_b,		togglebar,	{0} },
	{ MODKEY|ShiftMask,		XK_b,		spawn,		SHCMD("lutris lutris:rungameid/9") },
	{ MODKEY,		        XK_n,		spawn,		SHCMD("lutris lutris:rungameid/11") },
	/* { MODKEY|ShiftMask,             XK_n,		spawn,		SHCMD("lutris lutris:rungameid/45") }, */
	/* { MODKEY|ShiftMask,             XK_n,		spawn,		SHCMD("sudo -A sh -c 'sysctl -w abi.vsyscall32=0'") }, */
	{ MODKEY,			XK_m,		spawn,		SHCMD("st -e ncmpcpp") },
	{ MODKEY|ShiftMask,		XK_m,		spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ControlMask,	        XK_m,		spawn,		SHCMD("pamixer --source 1 -t") },

	/* { MODKEY,                       XK_comma,       focusmon,       {.i = -1 } }, */
	/* { MODKEY,                       XK_period,      focusmon,       {.i = +1 } }, */
	/* { MODKEY|ShiftMask,             XK_comma,       tagmon,         {.i = -1 } }, */
	/* { MODKEY|ShiftMask,             XK_period,      tagmon,         {.i = +1 } }, */

    /* Switching between monitors */
	{ MODKEY,			XK_Left,	focusmon,	{.i = -1 } },
	{ MODKEY|ShiftMask,		XK_Left,	tagmon,		{.i = -1 } },
	{ MODKEY,			XK_Right,	focusmon,	{.i = +1 } },
	{ MODKEY|ShiftMask,		XK_Right,	tagmon,		{.i = +1 } },

	/* { MODKEY,			XK_Page_Up,	shiftview,	{ .i = -1 } }, */
	/* { MODKEY|ShiftMask,		XK_Page_Up,	shifttag,	{ .i = -1 } }, */
	/* { MODKEY,			XK_Page_Down,	shiftview,	{ .i = +1 } }, */
	/* { MODKEY|ShiftMask,		XK_Page_Down,	shifttag,	{ .i = +1 } }, */
	{ MODKEY,			XK_Insert,	spawn,		SHCMD("notify-send \"📋 Clipboard contents:\" \"$(xclip -o -selection clipboard)\"") },

	
	{ MODKEY,			XK_F1,		spawn,		SHCMD("st -e castero") },
	{ MODKEY,			XK_F2,		spawn,		SHCMD("firefox") },
	{ MODKEY,			XK_F3,		spawn,		SHCMD("gimp") },
	{ MODKEY,			XK_F4,		spawn,		SHCMD("kdenlive") },
	/* { MODKEY,			XK_F4,		spawn,		SHCMD("export $(dbus-launch) && flatpak run org.kde.kdenlive") }, */
	{ MODKEY,			XK_F5,		xrdb,		{.v = NULL } },
	{ MODKEY,			XK_F6,		spawn,		SHCMD("steam") },
	{ MODKEY,			XK_F7,		spawn,		SHCMD("discord") },
	/* { MODKEY,			XK_F7,		spawn,		SHCMD("flatpak run com.discordapp.Discord") }, */
        { MODKEY,			XK_F8,		spawn,		SHCMD("obs -multi") },
	/* { MODKEY,			XK_F8,		spawn,		SHCMD("flatpak run com.obsproject.Studio") }, */
	{ MODKEY,			XK_F9,		spawn,		SHCMD("joplin-desktop") },
	/* { MODKEY,			XK_F9,		spawn,		SHCMD("dmenumount") }, */
	{ MODKEY,			XK_F10,		spawn,		SHCMD("qbittorrent") },
	/* { MODKEY,			XK_F10,		spawn,		SHCMD("dmenuumount") }, */
	{ MODKEY,			XK_F11,		spawn,		SHCMD("displayselect") },
	{ MODKEY,			XK_F12,		spawn,		SHCMD("dmenu-sysmon") },
        
	/* { Mod1Mask, 			XK_space,       spawn,		SHCMD("clayout") }, */
	/* { Mod1Mask, 			XK_space,       spawn,		SHCMD("clayout") }, */
	/* { MODKEY,			XK_space,	zoom,		{0} }, */
	{ MODKEY|ShiftMask,		XK_space,	togglefloating,	{0} },
        /* Screenshot with scrot */
	{ 0,			        XK_Print,	spawn,		SHCMD("scrot  '%Y-%m-%d-%H:%M:%S_$wx$h.png' -e 'mv $f ~/shots/' && notify-send 'Full Screenshot taken'") },
	{ ShiftMask,		        XK_Print,	spawn,		SHCMD("scrot  -u '%Y-%m-%d-%H:%M:%S_$wx$h.png' -e 'mv $f ~/shots/' && notify-send 'Full Window Screenshot taken'") },
	{ MODKEY|ShiftMask,		XK_Print,	spawn,		SHCMD("dmenu-scrot") },

	/* { MODKEY|ControlMask,		XK_Print,	spawn,		SHCMD("dmenurecord") }, */
	/* { MODKEY,			XK_Delete,	spawn,		SHCMD("dmenurecord kill") }, */
	{ MODKEY,			XK_Scroll_Lock,	spawn,		SHCMD("killall screenkey || screenkey &") },

	{ 0, XF86XK_AudioMute,		spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioRaiseVolume,	spawn,		SHCMD("pamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("pamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)") },
	/* { 0, XF86XK_AudioPrev,		spawn,		SHCMD("playerctl prev") }, */
	/* { 0, XF86XK_AudioNext,		spawn,		SHCMD("playerctl next") }, */
	{ 0, XF86XK_AudioPrev,		spawn,		SHCMD("mpc prev") },
	{ 0, XF86XK_AudioNext,		spawn,		SHCMD("mpc next") },
	{ 0, XF86XK_AudioPause,		spawn,		SHCMD("mpc pause") },
	{ 0, XF86XK_AudioPlay,          spawn,		SHCMD("mpc toggle") },
	{ 0, XF86XK_AudioStop,		spawn,		SHCMD("mpc stop") },
	/* { 0, XF86XK_AudioPlay,               spawn,		SHCMD("playerctl play-pause") }, */
	/* { 0, XF86XK_AudioStop,               spawn,		SHCMD("playerctl stop") }, */
	{ 0, XF86XK_AudioRewind,	spawn,		SHCMD("mpc seek -10") },
	{ 0, XF86XK_AudioForward,	spawn,		SHCMD("mpc seek +10") },
	{ 0, XF86XK_AudioMedia,		spawn,		SHCMD("st -e ncmpcpp") },
	{ 0, XF86XK_PowerOff,		spawn,		SHCMD("sysact") },
	{ 0, XF86XK_Calculator,		spawn,		SHCMD("qalculate-gtk") },
	{ 0, XF86XK_Sleep,		spawn,		SHCMD("sudo -A zzz") },
	{ 0, XF86XK_WWW,		spawn,		SHCMD("$BROWSER") },
	{ 0, XF86XK_DOS,		spawn,		SHCMD("st") },
	{ 0, XF86XK_ScreenSaver,	spawn,		SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_TaskPane,		spawn,		SHCMD("st -e htop") },
	{ 0, XF86XK_Mail,		spawn,		SHCMD("st -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,		spawn,		SHCMD("st -e vifm") },
	/* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
	{ 0, XF86XK_Launch1,		spawn,		SHCMD("xset dpms force off") },
	{ 0, XF86XK_TouchpadToggle,	spawn,		SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,	spawn,		SHCMD("synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOn,		spawn,		SHCMD("synclient TouchpadOff=0") },
	{ 0, XF86XK_MonBrightnessUp,	spawn,		SHCMD("brightnessctl set +10%") },
	{ 0, XF86XK_MonBrightnessDown,	spawn,		SHCMD("brightnessctl set 10%-") },

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkTagBar,		0,		Button4,	shiftview,	{.i = -1} },
	{ ClkTagBar,		0,		Button5,	shiftview,	{.i = 1} },
	{ ClkRootWin,		0,		Button2,	togglebar,	{0} },
};

