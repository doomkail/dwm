# Mi version de DWM (Dynamic Window Manager)

Mi version del Window Manager (Gestor de Ventanas)  DWM

Dwm es un gestor de ventanas rapido para X.Dwm fue creado por la gente de suckless.org. Esta es mi version de dwm.
Use unos parches en esta version:

* Statusbar:    Se puede interactuar con la barra de tareas con la version de dwmblocks de Luke Smith.
* attachaside:  los clientes nuevos apareceran en la pila y no en el area principal
* cyclelayouts:     rota a traves de todos los layouts disponibles
* restartsig:   permite reinicar dwm con un combinacion de teclas
* rotatestack:  mueve una ventana a traves de la pila en cualquier direccion
* pertag:   permite tener diferentes layouts entre cada tag
* systray:  activa la barra de tareas
* xrdb:     lee los colores/variables de Xresources
* actualfullscreen:     activa la pantalla completa con (super+f) y previene el cambio de focus
* swallow:  si un programa se esta ejecutando desde una terminal, toma temporalmente su lugar para ahorrar espacio
* shiftview:    Permite rotar entre tags con (alt+tab)
* sticky:   Ventana puede verse en todos los tags con (super + s)
* statusbarallmons: Permite ver la barra de tareas en todos los monitores

Esta version utiliza colores en los iconos es necesario instalar **libxft-bgra** desde AUR

Dependencias
* libxft-bgra
* libX11
* ttf-joypixels
* st
* dmenu
* make
* libxinerama
* freetype2
